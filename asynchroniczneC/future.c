#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include "future.h"
#include "err.h"

static void future_init(future_t *future) {
    int err;

    if ((err = pthread_mutex_init(&(future->mutex), NULL)) != 0) syserr(err, "pthread_mutex_init");
    if ((err = pthread_cond_init(&(future->end_cond), NULL) != 0)) syserr(err, "pthread_cond_init");

    future->end = false;
}

static void function_wrapper1(void *arg, size_t size) {
    if (size != sizeof(work_wrapper1_t)) {
        fprintf(stderr, "ERROR: Invalid arg size in function_wrapper1\n");

        return;
    }

    int err;
    work_wrapper1_t *work = arg;

    work->future->result = work->function(work->arg, work->argsz, &work->future->result_size);

    if ((err = pthread_mutex_lock(&(work->future->mutex))) != 0) syserr(err, "pthread_mutex_lock");

    work->future->end = true;

    if ((err = pthread_cond_signal(&(work->future->end_cond))) != 0) syserr(err, "pthread_cond_signal");
    if ((err = pthread_mutex_unlock(&(work->future->mutex))) != 0) syserr(err, "pthread_mutex_unlock");

    free(work);
}

static void function_wrapper2(void *arg, size_t size) {
    if (size != sizeof(work_wrapper2_t)) {
        fprintf(stderr, "ERROR: Invalid arg size in function_wrapper2\n");

        return;
    }

    int err;
    work_wrapper2_t *work = arg;
    void *result;

    result = await(work->future_from);

    work->future->result = work->function(result, work->future_from->result_size,
                                          &work->future->result_size);

    if ((err = pthread_mutex_lock(&(work->future->mutex))) != 0) syserr(err, "pthread_mutex_lock");

    work->future->end = true;

    if ((err = pthread_cond_signal(&(work->future->end_cond))) != 0) syserr(err, "pthread_cond_signal");
    if ((err = pthread_mutex_unlock(&(work->future->mutex))) != 0) syserr(err, "pthread_mutex_unlock");

    free(result);
    free(work);
}

static runnable_t create_runnable1(future_t *future, callable_t *callable) {
    runnable_t runnable;
    work_wrapper1_t *work_wrapper = malloc(sizeof(work_wrapper1_t));

    if (work_wrapper == NULL) {
        fprintf(stderr, "COULDN'T ALLOCATE MEMORY");
        exit(1);
    }

    work_wrapper->future = future;
    work_wrapper->function = callable->function;
    work_wrapper->arg = callable->arg;
    work_wrapper->argsz = callable->argsz;

    runnable.function = function_wrapper1;
    runnable.arg = work_wrapper;
    runnable.argsz = sizeof(work_wrapper1_t);

    return runnable;
}

static runnable_t
create_runnable2(future_t *future, future_t *future_from, void *(*function)(void *, size_t, size_t *)) {
    runnable_t runnable;
    work_wrapper2_t *work_wrapper = malloc(sizeof(work_wrapper2_t));

    if (work_wrapper == NULL) {
        fprintf(stderr, "COULDN'T ALLOCATE MEMORY");
        exit(1);
    }

    work_wrapper->future = future;
    work_wrapper->future_from = future_from;
    work_wrapper->function = function;

    runnable.function = function_wrapper2;
    runnable.arg = work_wrapper;
    runnable.argsz = sizeof(work_wrapper2_t);

    return runnable;
}

int async(thread_pool_t *pool, future_t *future, callable_t callable) {
    future_init(future);

    runnable_t runnable = create_runnable1(future, &callable);
    defer(pool, runnable);

    return 0;
}

int map(thread_pool_t *pool, future_t *future, future_t *from, void *(*function)(void *, size_t, size_t *)) {
    future_init(future);

    runnable_t runnable = create_runnable2(future, from, function);
    defer(pool, runnable);

    return 0;
}

void *await(future_t *future) {
    int err;

    if ((err = pthread_mutex_lock(&(future->mutex))) != 0) syserr(err, "pthread_mutex_lock");

    while (future->end == false)
        if ((err = pthread_cond_wait(&(future->end_cond), &(future->mutex))) != 0) syserr(err, "pthread_cond_wait");

    if ((err = pthread_mutex_unlock(&(future->mutex))) != 0) syserr(err, "pthread_mutex_unlock");

    if ((err = pthread_mutex_destroy(&(future->mutex))) != 0) syserr(err, "pthread_mutex_destroy");
    if ((err = pthread_cond_destroy(&(future->end_cond))) != 0) syserr(err, "pthread_cond_destroy");

    return future->result;
}