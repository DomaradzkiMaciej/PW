#ifndef ASYNCHRONICZNEC_FUTURE_H
#define ASYNCHRONICZNEC_FUTURE_H

#include <stddef.h>
#include "threadpool.h"

typedef struct callable {
    void *(*function)(void *, size_t, size_t *);

    void *arg;
    size_t argsz;
} callable_t;

typedef struct future {
    pthread_mutex_t mutex;
    pthread_cond_t end_cond;

    bool end;

    void *result;
    size_t result_size;
} future_t;

typedef struct work_wrapper1 {
    future_t *future;

    void *(*function)(void *, size_t, size_t *);

    void *arg;
    size_t argsz;
} work_wrapper1_t;

typedef struct work_wrapper2 {
    future_t *future;
    future_t *future_from;

    void *(*function)(void *, size_t, size_t *);
} work_wrapper2_t;

typedef struct future future_t;

int async(thread_pool_t *pool, future_t *future, callable_t callable);

int map(thread_pool_t *pool, future_t *future, future_t *from, void *(*function)(void *, size_t, size_t *));

void *await(future_t *future);

#endif //ASYNCHRONICZNEC_FUTURE_H
