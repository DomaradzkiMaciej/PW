#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "future.h"

typedef struct pair {
    int value;
    int time;
} pair_t;

static void *callable_fun(void *arg, size_t size, size_t *result_size) {
    if (size != sizeof(pair_t)) {
        fprintf(stderr, "ERROR: Invalid arg size in callable_fun\n");

        return NULL;
    }

    pair_t *pair = arg;
    int *result = malloc(sizeof(int));

    usleep(1000 * pair->time);

    *result = pair->value;
    *result_size = sizeof(int);

    return result;
}

int main() {
    int k, n;
    int sum = 0;
    int *result;

    thread_pool_t pool;
    callable_t callable;

    scanf("%i %i", &k, &n);
    thread_pool_init(&pool, 4);

    pair_t *pairs = malloc(k * n * sizeof(pair_t));
    future_t *futures = malloc(k * n * sizeof(future_t));

    for (int i = 0, x, y; i < k * n; ++i) {
        scanf("%i %i", &x, &y);

        pairs[i].value = x;
        pairs[i].time = y;
    }

    for (int i = 0; i < k * n; ++i) {
        callable.function = callable_fun;
        callable.arg = &pairs[i];
        callable.argsz = sizeof(pair_t);

        async(&pool, &futures[i], callable);
    }

    for (int i = 0, x = 1; i < k * n; ++i) {
        result = await(&futures[i]);
        sum += *result;

        free(result);

        if (x == n) {
            printf("%i\n", sum);

            x = 1;
            sum = 0;
        } else {
            x += 1;
        }
    }

    thread_pool_destroy(&pool);

    free(pairs);
    free(futures);

    return 0;
}