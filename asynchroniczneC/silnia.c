#include <stdio.h>
#include <stdlib.h>
#include "future.h"

typedef struct pair {
    int part_result;
    int multiplier;
} pair_t;

static void *callable_fun(void *arg, size_t size, size_t *result_size) {
    if (size != sizeof(pair_t)) {
        fprintf(stderr, "ERROR: Invalid arg size in callable_fun\n");

        return NULL;
    }

    pair_t *pair = arg;
    pair_t *result = malloc(sizeof(pair_t));

    result->part_result = pair->part_result * pair->multiplier;
    result->multiplier = pair->multiplier + 1;
    *result_size = sizeof(pair_t);

    return result;
}

int main() {
    int k;
    pair_t *result;
    pair_t pair;

    thread_pool_t pool;
    callable_t callable;

    scanf("%i", &k);
    thread_pool_init(&pool, 3);

    future_t *futures = malloc(k * sizeof(future_t));

    pair.part_result = 1;
    pair.multiplier = 1;

    callable.function = callable_fun;
    callable.arg = &pair;
    callable.argsz = sizeof(pair_t);

    async(&pool, &futures[0], callable);

    for (int i = 2; i <= k; ++i) {
        map(&pool, &futures[i - 1], &futures[i - 2], callable_fun);
    }

    result = await(&futures[k - 1]);

    printf("%i\n", result->part_result);

    thread_pool_destroy(&pool);

    free(result);
    free(futures);

    return 0;
}