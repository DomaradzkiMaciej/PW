#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "threadpool.h"
#include "err.h"

static work_t *work_create(runnable_t *runnable) {
    work_t *work;

    if (runnable == NULL)
        return NULL;

    work = malloc(sizeof(work_t));

    if (work == NULL) {
        fprintf(stderr, "COULDN'T ALLOCATE MEMORY");
        exit(1);
    }

    work->function = runnable->function;
    work->arg = runnable->arg;
    work->argsz = runnable->argsz;

    work->next = NULL;

    return work;
}

static void work_destroy(work_t *work) {
    free(work);
}

static work_t *get_work(thread_pool_t *pool) {
    work_t *work;

    if (pool == NULL || pool->work_first == NULL)
        return NULL;

    work = pool->work_first;

    if (work->next == NULL) {
        pool->work_first = NULL;
        pool->work_last = NULL;
    } else {
        pool->work_first = work->next;
    }

    return work;
}

static void *thread_pool_worker(void *arg) {
    int err;
    thread_pool_t *pool = arg;
    work_t *work;

    while (1) {
        if ((err = pthread_mutex_lock(&(pool->mutex))) != 0) syserr(err, "pthread_mutex_lock");

        if (pool->stop && pool->work_first == NULL)
            break;

        if (pool->work_first == NULL)
            if ((err = pthread_cond_wait(&(pool->is_work_cond), &(pool->mutex))) != 0) syserr(err, "pthread_cond_wait");

        work = get_work(pool);

        if ((err = pthread_mutex_unlock(&(pool->mutex))) != 0) syserr(err, "pthread_mutex_unlock");

        if (work != NULL) {
            work->function(work->arg, work->argsz);
            work_destroy(work);
        }
    }

    pool->thread_cnt--;
    if (pool->thread_cnt == 0)
        if ((err = pthread_cond_signal(&(pool->stop_cond))) != 0) syserr(err, "pthread_cond_signal");

    if ((err = pthread_mutex_unlock(&(pool->mutex))) != 0) syserr(err, "pthread_mutex_unlock");

    return NULL;
}

int thread_pool_init(thread_pool_t *pool, size_t pool_size) {
    int err;
    pthread_t thread;

    if (pool == NULL || pool_size <= 0)
        return -1;

    if ((err = pthread_mutex_init(&(pool->mutex), NULL)) != 0) syserr(err, "pthread_mutex_init");
    if ((err = pthread_cond_init(&(pool->is_work_cond), NULL) != 0)) syserr(err, "pthread_cond_init");
    if ((err = pthread_cond_init(&(pool->stop_cond), NULL) != 0)) syserr(err, "pthread_cond_init");

    pool->work_first = NULL;
    pool->work_last = NULL;

    pool->thread_cnt = pool_size;
    pool->stop = false;

    for (size_t i = 0; i < pool_size; i++) {
        if ((err = pthread_create(&thread, NULL, thread_pool_worker, pool)) != 0) syserr(err, "pthread_create");
        if ((err = pthread_detach(thread) != 0)) syserr(err, "pthread_detach");
    }

    return 0;
}

void thread_pool_destroy(thread_pool_t *pool) {
    int err;

    if (pool == NULL)
        return;

    if ((err = pthread_mutex_lock(&(pool->mutex))) != 0) syserr(err, "pthread_mutex_lock");

    pool->stop = true;

    if ((err = pthread_cond_broadcast(&(pool->is_work_cond))) != 0) syserr(err, "pthread_cond_broadcast");
    if ((err = pthread_cond_wait(&(pool->stop_cond), &(pool->mutex))) != 0) syserr(err, "pthread_cond_wait");
    if ((err = pthread_mutex_unlock(&(pool->mutex))) != 0) syserr(err, "pthread_mutex_unlock");

    usleep(1);

    if ((err = pthread_mutex_destroy(&(pool->mutex))) != 0) syserr(err, "pthread_mutex_destroy");
    if ((err = pthread_cond_destroy(&(pool->is_work_cond))) != 0) syserr(err, "pthread_cond_destroy");
    if ((err = pthread_cond_destroy(&(pool->stop_cond))) != 0) syserr(err, "pthread_cond_destroy");

}

int defer(thread_pool_t *pool, runnable_t runnable) {
    int err;
    work_t *work;

    if (pool == NULL)
        return -1;

    if ((err = pthread_mutex_lock(&(pool->mutex))) != 0) syserr(err, "pthread_mutex_lock");

    if (pool->stop == true)
        return -1;

    if ((err = pthread_mutex_unlock(&(pool->mutex))) != 0) syserr(err, "pthread_mutex_unlock");

    work = work_create(&runnable);
    if (work == NULL)
        return -1;

    if ((err = pthread_mutex_lock(&(pool->mutex))) != 0) syserr(err, "pthread_mutex_lock");

    if (pool->work_first == NULL) {
        pool->work_first = work;
        pool->work_last = work;
    } else {
        pool->work_last->next = work;
        pool->work_last = work;
    }

    if ((err = pthread_cond_broadcast(&(pool->is_work_cond))) != 0) syserr(err, "pthread_cond_broadcast");
    if ((err = pthread_mutex_unlock(&(pool->mutex))) != 0) syserr(err, "pthread_mutex_unlock");

    return 0;
}