#ifndef ASYNCHRONICZNEC_THREADPOOL_H
#define ASYNCHRONICZNEC_THREADPOOL_H

#include <pthread.h>
#include <stdbool.h>

typedef struct runnable {
    void (*function)(void *, size_t);

    void *arg;
    size_t argsz;
} runnable_t;

typedef struct work {
    void (*function)(void *, size_t);

    void *arg;
    size_t argsz;

    struct work *next;
} work_t;

typedef struct thread_pool {
    work_t *work_first;
    work_t *work_last;

    pthread_mutex_t mutex;
    pthread_cond_t is_work_cond;
    pthread_cond_t stop_cond;

    int thread_cnt;

    bool stop;
} thread_pool_t;

int thread_pool_init(thread_pool_t *pool, size_t pool_size);

void thread_pool_destroy(thread_pool_t *pool);

int defer(thread_pool_t *pool, runnable_t runnable);

#endif //ASYNCHRONICZNEC_THREADPOOL_H
