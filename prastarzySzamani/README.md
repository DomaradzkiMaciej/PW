# Prastarzy Szamani

## Wprowadzenie

Implementacja trzech algorytmów w C++ w sposób sekwencyjny oraz 
współbieżny, rozwiązujących następujące problemy:

- Dyskretny problem plecakowy
- Sortowanie przez scalanie (Merge sort)
- Znajdowanie elementu maksymalnego



