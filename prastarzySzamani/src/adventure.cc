#include "adventure.h"

void Adventure::merge(std::vector<GrainOfSand> &grains,
                      std::vector<GrainOfSand> &aux, uint64_t begin,
                      uint64_t middle, uint64_t end) {
  uint64_t i = begin;
  uint64_t j = middle + 1;
  uint64_t k = begin;

  while (i <= middle && j <= end) {
    if (grains[i] < grains[j]) {
      aux[k] = grains[i];
      ++i;
    } else {
      aux[k] = grains[j];
      ++j;
    }
    ++k;
  }

  while (i <= middle) {
    aux[k] = grains[i];
    ++k;
    ++i;
  }

  while (j <= end) {
    aux[k] = grains[j];
    ++k;
    ++j;
  }

  for (uint64_t l = begin; l <= end; l++) {
    grains[l] = aux[l];
  }
}

void Adventure::arrangeSand(std::vector<GrainOfSand> &grains,
                            std::vector<GrainOfSand> &aux, uint64_t begin,
                            uint64_t end) {
  if (begin < end) {
    uint64_t middle = (begin + end) / 2;

    arrangeSand(grains, aux, begin, middle);
    arrangeSand(grains, aux, middle + 1, end);

    merge(grains, aux, begin, middle, end);
  }
}

uint64_t LonesomeAdventure::packEggs(std::vector<Egg> &eggs,
                                     BottomlessBag &bag) {
  uint64_t eggsNr = eggs.size();
  uint64_t bagCapacity = bag.getCapacity();
  uint64_t weightOfEggsWithoutSize = 0;

  std::vector<std::vector<uint64_t>> aux(
      eggsNr + 1, std::vector<uint64_t>(bagCapacity + 1));

  for (uint64_t i = 0; i <= eggsNr; ++i) {
    if (i != 0 && eggs[i - 1].getSize() == 0)
      weightOfEggsWithoutSize = eggs[i - 1].getWeight();

    for (uint64_t j = 0; j <= bagCapacity; ++j) {
      if (i == 0)
        aux[i][j] = 0;
      else if (j == 0)
        aux[i][j] = weightOfEggsWithoutSize;
      else if (eggs[i - 1].getSize() <= j)
        aux[i][j] = std::max(
            eggs[i - 1].getWeight() + aux[i - 1][j - eggs[i - 1].getSize()],
            aux[i - 1][j]);
      else
        aux[i][j] = aux[i - 1][j];
    }
  }

  return aux[eggsNr][bagCapacity];
}

void LonesomeAdventure::arrangeSand(std::vector<GrainOfSand> &grains) {
  uint64_t size = grains.size();

  std::vector<GrainOfSand> aux(size);

  Adventure::arrangeSand(grains, aux, 0, size - 1);
}

Crystal LonesomeAdventure::selectBestCrystal(std::vector<Crystal> &crystals) {
  Crystal best;

  for (auto crystal : crystals) {
    if (best < crystal) best = crystal;
  }

  return best;
}

void TeamAdventure::pack(std::vector<std::vector<uint64_t>> &aux,
                         u_int64_t eggSize, uint64_t eggWeight, uint64_t i,
                         uint64_t begin, uint64_t end,
                         uint64_t weightOfEggsWithoutSize, uint64_t len,
                         uint64_t num) {
  if (begin >= end || num == 1) {
    for (uint64_t j = begin; j <= end; ++j) {
      if (i == 0)
        aux[i][j] = 0;
      else if (j == 0)
        aux[i][j] = weightOfEggsWithoutSize;
      else if (eggSize <= j)
        aux[i][j] =
            std::max(eggWeight + aux[i - 1][j - eggSize], aux[i - 1][j]);
      else
        aux[i][j] = aux[i - 1][j];
    }
  } else {
    uint64_t n = num / 2;
    uint64_t middle = begin + len * (num - n) - 1;

    auto promise = this->councilOfShamans.enqueue(
        &TeamAdventure::pack, this, std::ref(aux), eggSize, eggWeight, i, begin,
        middle, weightOfEggsWithoutSize, len, num - n);
    pack(aux, eggSize, eggWeight, i, middle + 1, end, weightOfEggsWithoutSize,
         len, n);

    promise.get();
  }
}

uint64_t TeamAdventure::packEggs(std::vector<Egg> &eggs, BottomlessBag &bag) {
  uint64_t eggsNr = eggs.size();
  uint64_t bagCapacity = bag.getCapacity();
  uint64_t weightOfEggsWithoutSize = 0;
  uint64_t len = bagCapacity / numberOfShamans;
  uint64_t num = bagCapacity % numberOfShamans;
  u_int64_t eggSize;
  uint64_t eggWeight;
  uint64_t middle = (len + 1) * num - 1;

  std::vector<std::vector<uint64_t>> aux(
      eggsNr + 1, std::vector<uint64_t>(bagCapacity + 1));

  for (uint64_t i = 0; i <= eggsNr; ++i) {
    if (i != 0 && eggs[i - 1].getSize() == 0)
      weightOfEggsWithoutSize = eggs[i - 1].getWeight();

    eggSize = (i == 0 ? 0 : eggs[i - 1].getSize());
    eggWeight = (i == 0 ? 0 : eggs[i - 1].getWeight());

    if (num == 0) {
      pack(aux, eggSize, eggWeight, i, 0, bagCapacity, weightOfEggsWithoutSize,
           len, numberOfShamans);
    } else {
      auto promise = this->councilOfShamans.enqueue(
          &TeamAdventure::pack, this, std::ref(aux), eggSize, eggWeight, i, 0,
          middle, weightOfEggsWithoutSize, len + 1, num);
      pack(aux, eggSize, eggWeight, i, middle + 1, bagCapacity,
           weightOfEggsWithoutSize, len, numberOfShamans - num);

      promise.get();
    }
  }

  return aux[eggsNr][bagCapacity];
}

void TeamAdventure::mergeSort(std::vector<GrainOfSand> &grains,
                              std::vector<GrainOfSand> &aux, uint64_t begin,
                              uint64_t end, uint64_t len, uint64_t num) {
  if (begin >= end) return;

  if (num == 1) {
    Adventure::arrangeSand(grains, aux, begin, end);
  } else {
    uint64_t n = num / 2;
    uint64_t middle = begin + len * (num - n) - 1;

    auto promise = this->councilOfShamans.enqueue(
        &TeamAdventure::mergeSort, this, std::ref(grains), std::ref(aux), begin,
        middle, len, num - n);
    mergeSort(grains, aux, middle + 1, end, len, n);

    promise.get();

    Adventure::merge(grains, aux, begin, middle, end);
  }
}

void TeamAdventure::arrangeSand(std::vector<GrainOfSand> &grains) {
  uint64_t size = grains.size();
  uint64_t len = size / numberOfShamans;
  uint64_t num = size % numberOfShamans;
  uint64_t middle = (len + 1) * num - 1;

  std::vector<GrainOfSand> aux(size);

  if (num == 0) {
    mergeSort(grains, aux, 0, size - 1, len, numberOfShamans);
  } else {
    auto promise = this->councilOfShamans.enqueue(
        &TeamAdventure::mergeSort, this, std::ref(grains), std::ref(aux), 0,
        middle, len + 1, num);
    mergeSort(grains, aux, middle + 1, size - 1, len, numberOfShamans - num);

    promise.get();

    Adventure::merge(grains, aux, 0, middle, size - 1);
  }
}

Crystal TeamAdventure::bestCrystal(const std::vector<Crystal> &crystals,
                                   uint64_t begin, uint64_t end, uint64_t len,
                                   uint64_t num) {
  if (begin >= end || num == 1) {
    Crystal best;

    for (uint64_t i = begin; i <= end; ++i) {
      if (best < crystals[i]) best = crystals[i];
    }

    return best;
  }

  uint64_t n = num / 2;
  uint64_t middle = begin + len * (num - n) - 1;

  auto promise = this->councilOfShamans.enqueue(&TeamAdventure::bestCrystal,
                                                this, std::cref(crystals),
                                                begin, middle, len, num - n);
  auto best1 = bestCrystal(crystals, middle + 1, end, len, n);
  auto best2 = promise.get();

  return (best1 < best2) ? best2 : best1;
}

Crystal TeamAdventure::selectBestCrystal(std::vector<Crystal> &crystals) {
  uint64_t size = crystals.size();
  uint64_t len = size / numberOfShamans;
  uint64_t num = size % numberOfShamans;
  uint64_t middle = (len + 1) * num - 1;

  if (num == 0) {
    return bestCrystal(crystals, 0, size - 1, len, numberOfShamans);
  } else {
    auto promise = this->councilOfShamans.enqueue(&TeamAdventure::bestCrystal,
                                                  this, std::cref(crystals), 0,
                                                  middle, len + 1, num);
    auto best1 =
        bestCrystal(crystals, middle + 1, size - 1, len, numberOfShamans - num);
    auto best2 = promise.get();

    return (best1 < best2) ? best2 : best1;
  }
}
