#ifndef SRC_ADVENTURE_H_
#define SRC_ADVENTURE_H_

#include <algorithm>
#include <vector>

#include "../third_party/threadpool/threadpool.h"

#include "./types.h"
#include "./utils.h"

class Adventure {
 public:
  virtual ~Adventure() = default;

  virtual uint64_t packEggs(std::vector<Egg> &eggs, BottomlessBag &bag) = 0;

  virtual void arrangeSand(std::vector<GrainOfSand> &grains) = 0;

  virtual Crystal selectBestCrystal(std::vector<Crystal> &crystals) = 0;

 protected:
  static void merge(std::vector<GrainOfSand> &grains,
                    std::vector<GrainOfSand> &aux, uint64_t begin,
                    uint64_t middle, uint64_t end);

  static void arrangeSand(std::vector<GrainOfSand> &grains,
                          std::vector<GrainOfSand> &aux, uint64_t begin,
                          uint64_t end);
};

class LonesomeAdventure : public Adventure {
 public:
  LonesomeAdventure() {}

  ~LonesomeAdventure() {}

  uint64_t packEggs(std::vector<Egg> &eggs, BottomlessBag &bag) override;

  void arrangeSand(std::vector<GrainOfSand> &grains) override;

  Crystal selectBestCrystal(std::vector<Crystal> &crystals) override;
};

class TeamAdventure : public Adventure {
 public:
  explicit TeamAdventure(uint64_t numberOfShamansArg)
      : numberOfShamans(numberOfShamansArg),
        councilOfShamans(numberOfShamansArg) {}

  uint64_t packEggs(std::vector<Egg> &eggs, BottomlessBag &bag) override;

  void arrangeSand(std::vector<GrainOfSand> &grains) override;

  Crystal selectBestCrystal(std::vector<Crystal> &crystals) override;

 private:
  void pack(std::vector<std::vector<uint64_t>> &aux, u_int64_t eggSize,
            uint64_t eggWeight, uint64_t i, uint64_t begin, uint64_t end,
            uint64_t weightOfEggsWithoutSize, uint64_t len, uint64_t num);

  void mergeSort(std::vector<GrainOfSand> &grains,
                 std::vector<GrainOfSand> &aux, uint64_t begin, uint64_t end,
                 uint64_t len, uint64_t num);

  Crystal bestCrystal(const std::vector<Crystal> &crystals, uint64_t begin,
                      uint64_t end, uint64_t len, uint64_t n);

  uint64_t numberOfShamans;
  ThreadPool councilOfShamans;
};

#endif  // SRC_ADVENTURE_H_
