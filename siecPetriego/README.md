# Sieć Petriego

## Wprowadzenie

Sieć Petriego to dwudzielny graf skierowany, którego węzłami są miejsca i przejścia. Miejsca i przejścia są połączone krawędziami.

W klasycznych sieciach Petriego występują krawędzie wejściowe, prowadzące z miejsca do przejścia, oraz krawędzie wyjściowe, prowadzące z przejścia do miejsca. Krawędzie wejściowe i wyjściowe mają dodatnie całkowite wagi.

W sieciach rozszerzonych, rozważanych w tym programie, z miejsca do przejścia mogą też prowadzić, nie posiadające wagi, krawędzie zerujące i krawędzie wzbraniające.

Znakowanie sieci przyporządkowuje miejscom nieujemną całkowitą liczbę żetonów.

W danym znakowaniu przejście jest dozwolone, jeśli:
- w każdym miejscu, z którym rozważane przejście jest połączone krawędzią wejściową, liczba żetonów jest większa lub równa wadze tej krawędzi, oraz
- w każdym miejscu, z którym rozważane przejście jest połączone krawędzią wzbraniającą, liczba żetonów jest równa zero.

Sieć Petriego ma stan, reprezentowany przez aktualne znakowanie. Stan sieci ulega zmianie w rezultacie odpalenia dozwolonego przejścia.

Odpalenie przejścia to operacja niepodzielna, powodująca wykonanie kolejno trzech kroków:
- usunięcia z każdego miejsca, z którym odpalane przejście jest połączone krawędzią wejściową tylu żetonów, jaka jest waga tej krawędzi,
- usunięcia wszystkich żetonów z każdego miejsca, z którym odpalane przejście jest połączone krawędzią zerującą,
- dodania do każdego miejsca, z którym odpalane przejście jest połączone krawędzią wyjściową tylu żetonów, jaka jest waga tej krawędzi.
