package alternator;

import java.util.*;

import petrinet.PetriNet;
import petrinet.Transition;

public class Main {

    private enum Place {
        A, B, C, MAIN
    }

    private static class Process implements Runnable {

        Place place1;
        Place place2;
        Place place3;

        PetriNet<Place> petriNet;

        private Process(Place place1, Place place2, Place place3, PetriNet<Place> petriNet) {
            this.place1 = place1;
            this.place2 = place2;
            this.place3 = place3;

            this.petriNet = petriNet;
        }

        @Override
        public void run() {
            Collection<Transition<Place>> transition1 = Collections.singleton(setTransitionMainX(place1));
            Collection<Transition<Place>> transition2 = Collections.singleton(setTransitionXMain(place1, place2, place3));

            while (true) {
                try {
                    if (Thread.interrupted()) {
                        return;
                    }

                    petriNet.fire(transition1);

                    System.out.println(place1);
                    System.out.println(".");

                    petriNet.fire(transition2);
                } catch (InterruptedException e) {
                    return;
                }
            }
        }
    }

    public static void main(String[] args) {
        try {
            PetriNet<Place> petriNet = setPetriNet();

            Thread[] processes = new Thread[3];
            processes[0] = new Thread(new Process(Place.A, Place.B, Place.C, petriNet));
            processes[1] = new Thread(new Process(Place.B, Place.C, Place.A, petriNet));
            processes[2] = new Thread(new Process(Place.C, Place.A, Place.B, petriNet));

            for (var process : processes) {
                process.start();
            }

            Thread.sleep(30000);

            for (var process : processes) {
                process.interrupt();
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static PetriNet<Place> setPetriNet() {
        Map<Place, Integer> net = new HashMap<>();
        net.put(Place.MAIN, 1);

        return new PetriNet<>(net, false);
    }

    private static Transition<Place> setTransitionMainX(Place X) {
        Map<Place, Integer> input = Collections.singletonMap(Place.MAIN, 1);
        Collection<Place> reset = new HashSet<>();
        Collection<Place> inhibitor = Collections.singleton(X);
        Map<Place, Integer> output = Collections.singletonMap(X, 2);

        return new Transition<>(input, reset, inhibitor, output);
    }

    private static Transition<Place> setTransitionXMain(Place X, Place Y, Place Z) {
        Map<Place, Integer> input = Collections.singletonMap(X, 1);
        Collection<Place> reset = new HashSet<>();
        reset.add(Y);
        reset.add(Z);
        Collection<Place> inhibitor = new HashSet<>();
        Map<Place, Integer> output = Collections.singletonMap(Place.MAIN, 1);

        return new Transition<>(input, reset, inhibitor, output);
    }
}
