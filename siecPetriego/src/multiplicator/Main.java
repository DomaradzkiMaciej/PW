package multiplicator;

import petrinet.PetriNet;
import petrinet.Transition;

import java.util.*;

public class Main {

    private enum Place {
        A, B, C, BUFFER_B, BUFFER_C, RESULT
    }

    private static class Process implements Runnable {

        PetriNet<Place> petriNet;
        Collection<Transition<Place>> transitions;
        int nr;
        int counter = 0;

        Process(PetriNet<Place> petriNet, Collection<Transition<Place>> transitions, int nr) {
            this.petriNet = petriNet;
            this.transitions = transitions;
            this.nr = nr;
        }

        @Override
        public void run() {
            while (true) {
                try {
                    petriNet.fire(transitions);
                    counter += 1;
                } catch (InterruptedException e) {
                    System.out.println("Watek " + nr + " - " + counter + " obliczen");
                    return;
                }
            }
        }
    }

    public static void main(String[] args) {
        try (Scanner scan = new Scanner(System.in)) {
            int A, B;
            A = scan.nextInt();
            B = scan.nextInt();

            PetriNet<Place> petriNet = setPetriNet(A, B);
            Collection<Transition<Place>> transitions = setTransitions();
            Thread[] processes = new Thread[4];

            for (int i = 0; i < 4; i++) {
                processes[i] = new Thread(new Process(petriNet, transitions, i));
                processes[i].start();
            }
            petriNet.fire(setTransitionsResult());
            System.out.println(petriNet.getValue(Place.RESULT));

            for (var process : processes) {
                process.interrupt();
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static PetriNet<Place> setPetriNet(int A, int B) {
        Map<Place, Integer> net = new HashMap<>();
        net.put(Place.A, A);
        net.put(Place.C, B);
        net.put(Place.BUFFER_C, 1);
        net.put(Place.RESULT, 0);

        return new PetriNet<>(net, true);
    }

    private static Collection<Transition<Place>> setTransitions() {
        Collection<Transition<Place>> transitions = new HashSet<>();
        transitions.add(setTransitionsB_BUFFER_C());
        transitions.add(setTransitionsB_RESULT());
        transitions.add(setTransitionsC_B());
        transitions.add(setTransitionsC_BUFFER_B());

        return transitions;
    }

    private static Transition<Place> setTransitionsC_B() {
        Map<Place, Integer> input = Collections.singletonMap(Place.C, 1);
        Collection<Place> reset = new HashSet<>();
        Collection<Place> inhibitor = Collections.singleton(Place.BUFFER_C);
        Map<Place, Integer> output = Collections.singletonMap(Place.B, 1);


        return new Transition<>(input, reset, inhibitor, output);
    }

    private static Transition<Place> setTransitionsB_RESULT() {
        Map<Place, Integer> input = Collections.singletonMap(Place.B, 1);
        Collection<Place> reset = new HashSet<>();
        Collection<Place> inhibitor = Collections.singleton(Place.BUFFER_B);
        Map<Place, Integer> output = new HashMap<>();
        output.put(Place.RESULT, 1);
        output.put(Place.C, 1);


        return new Transition<>(input, reset, inhibitor, output);
    }

    private static Transition<Place> setTransitionsC_BUFFER_B() {
        Map<Place, Integer> input = new HashMap<>();
        Collection<Place> reset = Collections.singleton(Place.BUFFER_B);
        Collection<Place> inhibitor = new HashSet<>();
        inhibitor.add(Place.C);
        inhibitor.add(Place.BUFFER_C);
        Map<Place, Integer> output = Collections.singletonMap(Place.BUFFER_C, 1);


        return new Transition<>(input, reset, inhibitor, output);
    }

    private static Transition<Place> setTransitionsB_BUFFER_C() {
        Map<Place, Integer> input = Collections.singletonMap(Place.A, 1);
        Collection<Place> reset = Collections.singleton(Place.BUFFER_C);
        Collection<Place> inhibitor = new HashSet<>();
        inhibitor.add(Place.B);
        inhibitor.add(Place.BUFFER_B);
        Map<Place, Integer> output = Collections.singletonMap(Place.BUFFER_B, 1);

        return new Transition<>(input, reset, inhibitor, output);
    }

    private static Collection<Transition<Place>> setTransitionsResult() {
        Collection<Transition<Place>> transition = new HashSet<>();

        Map<Place, Integer> input = new HashMap<>();
        Collection<Place> reset = new HashSet<>();
        Collection<Place> inhibitor = new HashSet<>();
        inhibitor.add(Place.A);
        inhibitor.add(Place.B);
        inhibitor.add(Place.BUFFER_B);

        Map<Place, Integer> output = new HashMap<>();

        transition.add(new Transition<>(input, reset, inhibitor, output));

        return transition;
    }
}
