package petrinet;

import java.util.*;
import java.util.concurrent.Semaphore;

public class PetriNet<T> {

    private final Map<T, Integer> net;
    private final Semaphore semaphore;

    public PetriNet(Map<T, Integer> initial, boolean fair) {
        this.net = initial;
        this.semaphore = new Semaphore(1, fair);
    }

    public Set<Map<T, Integer>> reachable(Collection<Transition<T>> transitions) {
        try {
            semaphore.acquire();
            Map<T, Integer> cNet = new HashMap<>(net);
            semaphore.release();

            Set<Map<T, Integer>> marking = new HashSet<>();
            reachable(marking, transitions, cNet);

            return marking;
        } catch (InterruptedException e) {
            System.err.println("InterruptedException");
            System.exit(1);
        } catch (StackOverflowError e) {
            System.err.println("StackOverflowError");
            System.exit(1);
        }

        return new HashSet<>();
    }

    public Transition<T> fire(Collection<Transition<T>> transitions) throws InterruptedException {
        while (true) {
            semaphore.acquire();

            for (var transition : transitions) {
                if (transition.isEnabled(net)) {
                    transition.fire(net);
                    semaphore.release();

                    return transition;
                }
            }

            semaphore.release();
        }
    }

    public int getValue(T place) {
        int value = 0;

        try {
            semaphore.acquire();
            value = net.get(place);
            semaphore.release();
        } catch (InterruptedException e) {
            System.err.println("InterruptedException");
            System.exit(1);
        }

        return value;
    }

    private void reachable(Set<Map<T, Integer>> marking, Collection<Transition<T>> transitions, Map<T, Integer> cNet) {
        if (marking.contains(cNet))
            return;

        marking.add(cNet);

        for (var transition : transitions) {
            if (transition.isEnabled(cNet)) {
                Map<T, Integer> mNet = new HashMap<>(cNet);
                transition.fire(mNet);
                reachable(marking, transitions, mNet);
            }
        }
    }
}