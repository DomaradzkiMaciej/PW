package petrinet;

import java.util.Collection;
import java.util.Map;

public class Transition<T> {

    private final Map<T, Integer> input;
    private final Collection<T> reset;
    private final Collection<T> inhibitor;
    private final Map<T, Integer> output;

    public Transition(Map<T, Integer> input, Collection<T> reset, Collection<T> inhibitor, Map<T, Integer> output) {
        this.input = input;
        this.reset = reset;
        this.inhibitor = inhibitor;
        this.output = output;
    }

    boolean isEnabled(Map<T, Integer> net) {
        for (var placeMap : input.entrySet()) {
            if (net.containsKey(placeMap.getKey()) && net.get(placeMap.getKey()) < placeMap.getValue())
                return false;
            if (!net.containsKey(placeMap.getKey()) && placeMap.getValue() > 0)
                return false;
        }

        for (var place : inhibitor) {
            if (net.containsKey(place) && net.get(place) != 0)
                return false;
        }

        return true;
    }

    void fire(Map<T, Integer> net) {
        for (var placeMap : input.entrySet()) {
            T place = placeMap.getKey();
            int value = placeMap.getValue();

            if (!net.containsKey(place) && value > 0) {
                System.err.println("This should't happen, it should be check by method isEnable");
                System.exit(1);
            }

            if (value > 0 && net.get(place) == value)
                net.remove(place);
            else if (value > 0)
                net.put(place, net.get(place) - value);
        }

        for (var place : reset)
            net.remove(place);

        for (var placeMap : output.entrySet()) {
            T place = placeMap.getKey();
            int value = placeMap.getValue();

            if (net.containsKey(place))
                net.put(place, net.get(place) + value);
            else
                net.put(place, value);
        }
    }
}